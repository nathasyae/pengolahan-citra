#!/usr/bin/env python
# coding: utf-8

# In[13]:


# convert to greyscale
from matplotlib import pyplot as plt
from PIL import Image

i0 = Image.open('ueno.jpg')
i1 = i0.convert('L')
i1.save('ueno_greyscale.jpg')


plt.subplot(1,2,1); plt.imshow(i0)
plt.title('Original'); plt.axis("off")
plt.subplot(1,2,2); plt.imshow(i1, cmap='gray')
plt.title('Greyscale'); plt.axis("off")
plt.show()


# In[37]:


# plot histogram
from skimage import util, color
import numpy as np

gray = util.img_as_ubyte(i1)
plt.subplot(1,2,1); plt.hist(gray.flatten(), 256, range=(0,255))
plt.show()

# max
arr = np.array(gray)
print(np.where(arr == arr.max()))
print(arr.max())
print(np.bincount(arr.flatten()).argmax())


# In[15]:


# contrast stretching
from skimage import io, util
import numpy as np
from matplotlib import pyplot as plt

i3 = io.imread('ueno_greyscale.JPG')
mn = min(i3.flatten())
mx = max(i3.flatten())
b = int(np.floor(255 / (mx - mn)))
i3_cs = (i3 - mn) * b

plt.subplot(1,2,1); plt.imshow(i3, cmap='gray')
plt.title('Original'); plt.axis("off")
plt.subplot(1,2,2); plt.imshow(i3_cs, cmap='gray')
plt.title('Contrast Stretching'); plt.axis("off")
plt.show()

gray = util.img_as_ubyte(i3)
plt.subplot(2,2,1); plt.hist(gray.flatten(), 256, range=(0,255))
gray2 = util.img_as_ubyte(i3_cs)
plt.subplot(2,2,2); plt.hist(gray2.flatten(), 256, range=(0,255))
plt.show()


# In[16]:


# histogram equalizer
from skimage import exposure

i1 = io.imread('ueno_greyscale.JPG')
i1_eq = exposure.equalize_hist(i1)

plt.subplot(1,2,1); plt.imshow(i1,cmap="gray")
plt.title('Original'); plt.axis("off")
plt.subplot(1,2,2); plt.imshow(i1_eq,cmap="gray")
plt.title('Histogram Equalizer'); plt.axis("off")
plt.show()

gray = util.img_as_ubyte(i1)
plt.subplot(2,2,1); plt.hist(gray.flatten(), 256, range=(0,255))
gray2 = util.img_as_ubyte(i1_eq)
plt.subplot(2,2,2); plt.hist(gray2.flatten(), 256, range=(0,255))
plt.show()


# In[17]:


# ecctv bisa dikit
from skimage import io, util, exposure, color, filters, morphology
from matplotlib import pyplot as plt
from PIL import Image
from skimage import img_as_ubyte

ori = io.imread('cctv.JPG')

equi = exposure.equalize_hist(ori)

noise=util.img_as_ubyte(util.random_noise(equi, mode='s&p',salt_vs_pepper=0.02))
fi=filters.rank.median(noise,selem=morphology.square(2))

negatif = 255-fi

sh = filters.unsharp_mask(negatif, radius=2)


io.imsave('finalcctv.png', sh)

# show
plt.subplot(); plt.imshow(ori, cmap='gray')
plt.title('Original'); plt.axis("off")
plt.show()
plt.subplot(); plt.imshow(equi, cmap='gray')
plt.title('Equi'); plt.axis("off")
plt.show()
plt.subplot(); plt.imshow(fi, cmap='gray')
plt.title('Median Filter'); plt.axis("off")
plt.show()
plt.subplot(); plt.imshow(negatif, cmap='gray')
plt.title('Negative'); plt.axis("off")
plt.show()
plt.subplot(); plt.imshow(sh, cmap='gray')
plt.title('Final'); plt.axis("off")
plt.show()


# In[18]:


# Lake

from skimage.filters import threshold_otsu
RGB = io.imread('teles2010.jpg')
i00 = io.imread('teles2000.jpg')

plt.subplot(1,2,1);plt.imshow(RGB)
plt.title('original'); plt.axis("off")

plt.subplot(1,2,2);plt.imshow(i00)
plt.title('background ori'); plt.axis("off")
plt.show()

R = RGB[:,:,0]
i00_gray = i00[:,:,0]
# i00_gray = color.rgb2gray(i00)
plt.subplot(1,2,1);plt.imshow(R,cmap='gray',vmin=0,vmax=255)
plt.title('fore Greyscale'); plt.axis("off")
plt.subplot(1,2,2);plt.imshow(i00_gray,cmap='gray',vmin=0,vmax=255)
plt.title('bg Greyscale'); plt.axis("off")
plt.show()


fi1 = filters.rank.mean(R, selem=morphology.square(8))
fi2 = filters.rank.mean(i00_gray, selem=morphology.square(8))
sh1 = filters.unsharp_mask(fi1, radius=3)
sh2 = filters.unsharp_mask(fi2, radius=3)

plt.subplot(1,2,1); plt.imshow(fi1, cmap='gray')
plt.title('fore blurred'); plt.axis("off")
plt.subplot(1,2,2); plt.imshow(fi2, cmap='gray')
plt.title('bg blurred'); plt.axis("off")
plt.show()

plt.subplot(1,2,1); plt.imshow(sh1, cmap='gray')
plt.title('fore sharpened'); plt.axis("off")
plt.subplot(1,2,2); plt.imshow(sh2, cmap='gray')
plt.title('bg sharpened'); plt.axis("off")
plt.show()


# i_subs = np.subtract(R,i00_gray)
i_subs = np.subtract(sh1,sh2)

thresh = threshold_otsu(i_subs)
final = i_subs > thresh

plt.subplot(1,2,1); plt.imshow(i_subs, cmap='gray')
plt.title('Substraction'); plt.axis("off")
plt.subplot(1,2,2); plt.imshow(final, cmap='gray')
plt.title('Output'); plt.axis("off")
plt.show()


# In[33]:


# Beach

import numpy as np
from skimage import io
from skimage.filters import threshold_otsu

RGB = io.imread('pantai1.jpg')
i00 = io.imread('pantai2.jpg')

plt.subplot(1,2,1);plt.imshow(RGB)
plt.title('original'); plt.axis("off")

plt.subplot(1,2,2);plt.imshow(i00)
plt.title('background ori'); plt.axis("off")
plt.show()


mn = min(RGB.flatten())
mx = max(RGB.flatten())
b = int(np.floor(255 / (mx - mn)))
RGB_cs = (RGB - mn) * b

mn = min(i00.flatten())
mx = max(i00.flatten())
b = int(np.floor(255 / (mx - mn)))
i00_cs = (i00 - mn) * b

plt.subplot(1,2,1); plt.imshow(RGB_cs)
plt.title('Contrast Stretching'); plt.axis("off")
plt.subplot(1,2,2); plt.imshow(i00_cs)
plt.title('Contrast Stretching'); plt.axis("off")
plt.show()

i_subs=np.subtract(color.rgb2gray(RGB_cs),color.rgb2gray(i00_cs))

thresh = threshold_otsu(i_subs)
final = i_subs < thresh

plt.subplot(1,2,1); plt.imshow(i_subs, cmap='gray')
plt.title('Substraction'); plt.axis("off")
plt.subplot(1,2,2); plt.imshow(final, cmap='gray')
plt.title('Output'); plt.axis("off")
plt.show()

